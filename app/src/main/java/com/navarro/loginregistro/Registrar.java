package com.navarro.loginregistro;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class Registrar extends AppCompatActivity {
    EditText name, aPaterno, aMaterno, emailtxt, celular;
    CardView btnRegistrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);

        btnRegistrar = findViewById(R.id.registrarUsuario);
        name = findViewById(R.id.txtNombreUsuario);
        aPaterno = findViewById(R.id.txtApellidoPaterno);
        aMaterno = findViewById(R.id.txtApellidoMaterno);
        emailtxt = findViewById(R.id.txtEmail);
        celular = findViewById(R.id.txtCelular);


        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    registrarUsuario();//Llamando a la funcion de la petición
            }
        });

    }

    //Realiza Perición a webService
    public void registrarUsuario() {
        MediaType MEDIA_TYPE = MediaType.parse("application/json");
        //Url a donde se hara la peticion
        String url = "http://grancompu.mx/evento/concierto/registra/checkin";
        final OkHttpClient client = new OkHttpClient();

        //Obtiene datos del EditText directamente
        String nombre = name.getText().toString();
        String apPaterno = aPaterno.getText().toString();
        String apMaterno = aMaterno.getText().toString();
        String email = emailtxt.getText().toString();
        String cel = celular.getText().toString();

        //Se crea el jsonObject y se envia
        JSONObject postdata = new JSONObject();
        try {
            // key" " y valor" " de JsonObject
            postdata.put("nombre", nombre);
            postdata.put("apellidoPaterno", apPaterno);
            postdata.put("apellidoMaterno",apMaterno);
            postdata.put("email",email);
            postdata.put("celular",cel);

        } catch(JSONException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MEDIA_TYPE, postdata.toString());
        final Request request = new Request.Builder()
                .url(url)
                .post(body)//tipo de peticion POST,GET o PUT
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                String mMessage = e.getMessage().toString();
                Log.w("failure Response", mMessage);
                //call.cancel();
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Error al Registrar el Usuario" , Toast.LENGTH_LONG).show();
                    }
                });
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String mMessage = response.body().string();
                Log.e(TAG, mMessage);
                try {
                    //Respuesta de la peticion realizada
                    response = client.newCall(request).execute();
                    String resultado = response.body().string();
                    response.message();
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Usuario Registrado", Toast.LENGTH_LONG).show();
                        }
                    });

                } catch (IOException e) {
                    e.printStackTrace();
                }
//

            }
        });

    }
}
