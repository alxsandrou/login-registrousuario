package com.navarro.loginregistro;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class MainActivity extends AppCompatActivity {

    private CardView Login;
    private EditText edtUsername, edtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Login = findViewById(R.id.Login);
        edtUsername = findViewById(R.id.edtUsername);
        edtPassword = findViewById(R.id.edtPassword);

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //se obtiene el texto del editText =>
                String usuario = edtUsername.getText().toString();
                String contra = edtPassword.getText().toString();
                //funcion consulta de login usuario
               // consultarUsuario(usuario,contra);
                if (usuario.isEmpty() && contra.isEmpty()) {
                    Toast.makeText(getApplicationContext(),
                            "Campos requeridos", Toast.LENGTH_LONG).show();

                } else {
                    //Valida si no estan vacios o puede validar con una consulta a WebService.. y lo manda a la otra Pantalla

                    if (usuario.equals("admin") && contra.equals("admin")) {
                        Intent intent = new Intent(MainActivity.this, Registrar.class);
                        startActivityForResult(intent, 0);
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(),
                                "Datos incorrectos", Toast.LENGTH_LONG).show();
                    }

                }


            }
        });

    }
    //Funcion para hacer la consulta del usuario

   /* public void  consultarUsuario(String usuario,String password) {
        MediaType MEDIA_TYPE = MediaType.parse("application/json");
        //Url a donde se hara la peticion
        String url = "http://........................................";
        final OkHttpClient client = new OkHttpClient();

        //Obtiene datos del EditText directamente
      String nombre=null;
      String pass= null;
        //Se crea el jsonObject y se envia
        JSONObject postdata = new JSONObject();
        try {
            // key" " y valor" " de JsonObject
            postdata.put("username", nombre);
            postdata.put("password",pass);


        } catch(JSONException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MEDIA_TYPE, postdata.toString());
        final Request request = new Request.Builder()
                .url(url)
                .post(body)//tipo de peticion POST,GET o PUT
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                String mMessage = e.getMessage().toString();
                Log.w("failure Response", mMessage);

            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String mMessage = response.body().string();
                Log.e(TAG, mMessage);
                try {
                    //Respuesta de la peticion realizada
                    response = client.newCall(request).execute();
                    String resultado = response.body().string();
                    response.message();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }*/
}
